import sys
import requests
import cfscrape
import urllib3
from urllib3 import exceptions
import sqlite3
import os
from datetime import datetime
currentdir = os.path.dirname(os.path.realpath(__file__))
db_path = os.path.join(currentdir, "samolet.db")
sys.path.append(db_path)
sys.path.append("/home/manage_report")

from Send_report.mywrapper import magicDB

class Parser:
    def __init__(self, parser_name: str):
        self.session = cfscrape.create_scraper(sess=requests.Session())
        self.result_data: dict = {'name': parser_name,
                                  'data': []}

    @magicDB
    def run(self):
        content: list = self.get_data()
        self.result_data['data'] = content
        print(content)
        return self.result_data

    def create_database(self):
        try:
            conn = sqlite3.connect(db_path)
            sql = conn.cursor()
            sql.execute("""CREATE TABLE orders (url text);""")
            conn.close()
        except:
            pass

    def insert_data(self, value):
        conn = sqlite3.connect(db_path)
        c = conn.cursor()
        c.execute("INSERT INTO orders(url) VALUES (?)", [value])
        conn.commit()
        conn.close()

    def search_data(self, search_value):
        conn = sqlite3.connect(db_path)
        c = conn.cursor()
        c.execute("SELECT * FROM orders WHERE url = ?", (search_value,))
        rows = c.fetchall()
        if rows:
            conn.close()
            return True
        else:
            conn.close()
            return False

    def get_ads(self):
        ads = []
        urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
        session = requests.Session()
        url = 'https://partner.samolet.ru/api/tender/tenders/?limit=50&page=1&state=PUBLISHED&state=CONFIRM_PARTICIPATION' \
              '&state=ACCEPT_PROPOSALS&state=ACCEPT_PROPOSALS_FINISHED&state=ANALYSIS&state=RESULTS&state=ADMIT_PARTICIPANTS'
        session.headers.update({
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36 Edg/107.0.1418.56'
        })
        session.get('https://partner.samolet.ru/', verify=False)

        response = session.get(url, verify=False)
        data = response.json()
        for item in data.get('results'):
            ads.append(item.get('id'))
        return ads

    def get_data(self):
        self.create_database()
        ads = self.get_ads()
        data = []
        for tender_id in ads:
            db_search = self.search_data(tender_id)
            if db_search:
                continue
            else:
                content = self.get_content(tender_id)
                data.append(content)
                self.insert_data(tender_id)
        return data

    def get_content(self, tender_id):
        content = {}
        try:
            session = requests.Session()
            url = f'https://partner.samolet.ru/api/tender/tenders/{tender_id}/'
            session.headers.update({
                'Accept': 'application/json',
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36 Edg/109.0.1518.61'
            })

            response = session.get(url, verify=False)
            view_data = response.json()

            item_data = {
                'type': 2,
                'title': '',
                'purchaseNumber': '',
                'fz': 'Коммерческие',
                'purchaseType': '',
                'url': '',
                'lots': [],
                'attachments': [],
                'procedureInfo': {
                    'endDate': '',
                    'scoringDate': '',
                    'biddingDate': ''
                },

                'customer': {
                    'fullName': '',
                    'factAddress': '',
                    'inn': 0,
                    'kpp': 0,
                },
                'contactPerson': {
                    'lastName': '',
                    'firstName': '',
                    'middleName': '',
                    'contactPhone': '',
                    'contactEMail': '',
                }
            }
            item_data['title'] = str(self.get_title(view_data))
            item_data['url'] = str(self.get_url(tender_id))

            item_data['purchaseType'] = str(self.get_type(view_data))
            item_data['purchaseNumber'] = str(tender_id)

            item_data['customer']['fullName'] = str(self.get_customer(view_data))
            item_data['customer']['inn'] = int(self.get_inn(view_data))
            item_data['customer']['kpp'] = int(self.get_kpp(view_data))

            item_data['procedureInfo']['endDate'] = self.get_end_date(view_data)
            item_data['procedureInfo']['scoringDate'] = self.get_scor_date(view_data)

            last_name, first_name, middle_name = self.get_contact(view_data)
            item_data['contactPerson']['lastName'] = str(last_name)
            item_data['contactPerson']['firstName'] = str(first_name)
            item_data['contactPerson']['middleName'] = str(middle_name)

            item_data['contactPerson']['contactPhone'] = str(self.get_phone(view_data))
            item_data['contactPerson']['contactEMail'] = str(self.get_email(view_data))

            item_data['lots'] = self.get_lots(view_data)
            item_data['attachments'] = self.get_docs(view_data)

            content = item_data
        except Exception as e:
            print(e)
            print("Страница с ошибкой ", tender_id)

        return content

    def get_title(self, view_data):
        try:
            data = view_data.get('name')
        except Exception as e:
            print(e)
            data = ''
        return data

    def get_type(self, view_data):
        try:
            data = 'Закупка ТМЦ'
        except:
            data = ''
        return data

    def get_number(self, view_data):
        try:
            data = view_data.get('number')
        except:
            data = ''
        return data

    def get_url(self, tender_id):
        try:
            url = 'https://partner.samolet.ru/tenders/' + str(tender_id)
        except:
            url = ''
        return url

    def get_contact(self, view_data):
        try:
            last_name = view_data.get("users")[0].get('lastName')
        except:
            last_name = ''
        try:
            first_name = view_data.get("users")[0].get('firstName')
        except:
            first_name = ''
        try:
            middle_name = view_data.get("users")[0].get('patronymic')
        except:
            middle_name = ''

        return last_name, first_name, middle_name

    def get_phone(self, view_data):
        try:
            data = ''.join(view_data.get("users")[0].get('workPhones'))
        except:
            data = ''
        return data

    def get_email(self, view_data):
        try:
            data = view_data.get("users")[0].get('email')
        except:
            data = ''
        return data

    def get_customer(self, view_data):
        try:
            data = view_data.get('authorities')[0].get('name')
        except:
            data = ''
        return data

    def get_inn(self, view_data):
        try:
            data = view_data.get('authorities')[0].get('inn')
            if not data or data is None:
                data = 0
        except:
            data = 0
        return data

    def get_kpp(self, view_data):
        try:
            data = view_data.get('authorities')[0].get('kpp')
            if not data or data is None:
                data = 0
        except:
            data = 0
        return data

    def get_end_date(self, view_data):
        try:
            old_date = view_data.get('confirmationEndDt')
            date = self.formate_date(old_date)
        except:
            date = None
        return date

    def get_scor_date(self, view_data):
        try:
            old_date = view_data.get('tenderFinishDt')
            date = self.formate_date(old_date)
        except:
            date = None
        return date

    def get_lots(self, view_data):
        data = []
        try:
            lots = {
                'region': '',
                'address': '',
                'price': '',
                'lotItems': []

            }
            for lot in view_data.get('lots'):
                names = {'code': str(lot.get('order')),
                         'name': ' '.join(lot.get('name').split())}
                lots['lotItems'].append(names)

            region = (', '.join(view_data.get('region')))
            lots['region'] += str(region)
            data.append(lots)
        except:
            return []
        return data

    def get_docs(self, view_data):
        docs = []
        try:
            for doc in view_data.get('designSpecification'):
                doc_list = {'docDescription': str(doc.get('name')), 'url': str(doc.get('document'))}
                docs.append(doc_list)
        except:
            pass

        try:
            for doc in view_data.get('additionalDocumentation'):
                doc_list = {'docDescription': str(doc.get('name')), 'url': str(doc.get('document'))}
                docs.append(doc_list)
        except:
            pass
        return docs

    def formate_date(self, old_date):
        date_object = datetime.strptime(old_date, '%Y-%m-%dT%H:%M:%SZ')
        formatted_date = date_object.strftime('%H.%M.%S %d.%m.%Y')
        return formatted_date
